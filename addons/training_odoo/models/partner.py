import string
from odoo import models, fields, api

class Partner(models.Model):
    _inherit = 'res.partner'

    session_line = fields.One2many('training.session', 'partner_id', string='Daftar Mengajar Sesi', readonly=True)
    # instruktur = fields.Boolean(string='Instruktur')